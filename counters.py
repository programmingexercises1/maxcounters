def solution(N, A):
    # write your code in Python 3.6
    counters = [0] * N
    max_count = 0
    last_update = 0

    for X in A:
        if X <= N:
            if counters[X - 1] < last_update:
                counters[X - 1] = last_update

            counters[X - 1] += 1

            if counters[X - 1] > max_count:
                max_count = counters[X - 1]
        else:
            last_update = max_count

    for i in range(N):
        if counters[i] < last_update:
            counters[i] = last_update

    return counters

print(solution(5, [3, 4, 4, 6, 1, 4, 4]))